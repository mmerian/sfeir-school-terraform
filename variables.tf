variable "application_name" {
  type    = string
  default = "mmerian-step-07"
}
variable "machine_type" {
  type    = string
  default = "e2-micro"
}
variable "gcp_project" {
  type    = string
  default = "sfeir-school-terraform"
}
