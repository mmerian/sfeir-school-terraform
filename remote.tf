terraform {
  backend "gcs" {
    // Update value with the gcs bucket used to save state files
    bucket = "mmerian-formation-tf-20211118"
  }
}